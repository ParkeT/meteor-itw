const Messages = new Mongo.Collection("messages");

if (Meteor.isServer) {

    Meteor.publish("mess", () => Messages.find());

    Messages.allow({
        insert() {
            return true;
        }
    });
}


if (Meteor.isClient) {
    let inputValue = "";

    Meteor.subscribe("mess");

    Template.chat.helpers({
        messages() {
          return Messages.find({}, {
            sort: { created: -1 },
            limit: 50
          });
        }
    });

    Template.chat.events({
        "click #send"() {
            Messages.insert({
                message: inputValue,
                created: Date.now()
            });               
        },
        "change #inputField"(ev) {
            inputValue = ev.target.value;
        }
    });
}



